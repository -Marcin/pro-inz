#ifndef ZDEFINIOWANE_H
#define ZDEFINIOWANE_H
#include "jednostkiSI.h"

namespace SI {

//definiowanie podstawowych jednostek ukladu SI
class Czas final: public Podstawowa {
protected:
    static const short int identyfikator = 1;
public:
    Czas() : Podstawowa("SEKUNDA", "s", "second") {}
    Czas(int _w) : Czas() {
        this->setWykladnik(_w);
    }
    short int getId() const {
        return Czas::identyfikator;
    }
};
class Masa final: public Podstawowa {
protected:
    static const short int identyfikator = 2;
public:
    Masa()  : Podstawowa("KILOGRAM", "kg") {}
    Masa(int _w) : Masa() {
        this->setWykladnik(_w);
    }
    short int getId() const {
        return Masa::identyfikator;
    }
};
class Temperatura final: public Podstawowa {
protected:
    static const short int identyfikator = 3;
public:
    Temperatura()  : Podstawowa("KELWIN", "K", "kelvin") {}
    Temperatura(int _w) : Temperatura() {
        this->setWykladnik(_w);
    }
    short int getId() const {
        return Temperatura::identyfikator;
    }
};
class Prad final: public Podstawowa {
protected:
    static const short int identyfikator = 4;
public:
    Prad()  : Podstawowa("AMPER", "A", "ampere") {}
    Prad(int _w) : Prad() {
        this->setWykladnik(_w);
    }
    short int getId() const {
        return Prad::identyfikator;
    }
};
class Dlugosc final: public Podstawowa {
protected:
    static const short int identyfikator = 5;
public:
    Dlugosc()  : Podstawowa("METR", "m", "meter") {}
    Dlugosc(int _w) : Dlugosc() {
        this->setWykladnik(_w);
    }
    short int getId() const {
        return Dlugosc::identyfikator;
    }
};
class LicznoscMaterii final: public Podstawowa {
protected:
    static const short int identyfikator = 6;
public:
    LicznoscMaterii()  : Podstawowa("MOL", "mol", "mole") {}
    LicznoscMaterii(int _w) : LicznoscMaterii() {
        this->setWykladnik(_w);
    }
    short int getId() const {
        return LicznoscMaterii::identyfikator;
    }
};
class Swiatlosc final: public Podstawowa {
protected:
    static const short int identyfikator = 7;
public:
    Swiatlosc()  : Podstawowa("KANDELA", "cd", "candela") {}
    Swiatlosc(int _w) : Swiatlosc() {
        this->setWykladnik(_w);
    }
    short int getId() const {
        return Swiatlosc::identyfikator;
    }
};
}
#endif // ZDEFINIOWANE_H
