//plik nagłówkowy opisujący interfejsy klas
#ifndef INTERFEJSY_H
#define INTERFEJSY_H
#include <iostream>

using namespace std;

namespace SI {

template <typename W, typename J>
class WzorzecWartosci {
protected:
    W wartosc;
    J jednostka;
    virtual WzorzecWartosci* pomnoz(WzorzecWartosci *wart) = 0;
    virtual WzorzecWartosci* dodaj(WzorzecWartosci *wart) = 0;
    virtual string toString();

    template <typename T_WART, typename T_JEDN>
    WzorzecWartosci<T_WART, T_JEDN> pomnoz(WzorzecWartosci<T_WART, T_JEDN> wart) {
        WzorzecWartosci<T_WART, T_JEDN> wynik = WzorzecWartosci<T_WART, T_JEDN>();
        wynik.jednostka = this->jednostka * wart.jednostka;
        wynik.wartosc = this->wartosc * wart.wartosc;
        return wynik;
    }

public:
    virtual ~WzorzecWartosci();
    friend ostream &operator<< (ostream &wyjscie, WzorzecWartosci* wart) {
        return wyjscie << wart->toString();
    }

    W getWartosc();
    void setWartosc(W wartosc);
    J getJednostka();
    void setJednostka(J jedn);

};

class Jednostka {
protected:
    string nazwa;
    string skrot;
    string nazwaMiedzynarodowa;
    int wykladnik;

    Jednostka(string _nazwa, string _skrot, string _nazwaMiedzynarodowa);


public:
    Jednostka();
    Jednostka(const Jednostka &jedn);
    virtual ~Jednostka();
    string getNazwa();
    string getSkrot();
    int getWykladnik();
    virtual string toString();

    void setWykladnik(int _wykladnik);

    virtual bool czyMoznaSumowac(Jednostka *jedn);
    friend ostream &operator<< (ostream &wyjscie, Jednostka &jedn);
    friend ostream &operator<< (ostream &wyjscie, Jednostka *jedn);

    virtual Jednostka* pomnoz(Jednostka *jedn);
    virtual Jednostka* dodaj(Jednostka *jedn) = 0;

    Jednostka* operator*(Jednostka &jedn);
    Jednostka* operator/(Jednostka &jedn);
    Jednostka* operator+(Jednostka &jedn);
    Jednostka* operator-(Jednostka &jedn);
    Jednostka* operator^ (int _wykladnik);
};

class Podstawowa : public Jednostka {
protected:
    static const short int identyfikator = 0;
    Podstawowa(string _nazwa, string _skrot, string _nazwaMiedzynarodowa);
public:
    virtual short int getId() const {
        return Podstawowa::identyfikator;
    }

    Podstawowa(const Podstawowa &podstawowa);
    Podstawowa();

    //operator porownania
    bool operator== (const Podstawowa *podstawowa) const;

    //metoda sprawdzajaca czy jednostka jest tego samego typu
    bool is(const Podstawowa *podstawowa) const;

    Podstawowa* operator=(Podstawowa &jedn);
    Podstawowa* operator=(Podstawowa *jedn);

    Jednostka* pomnoz(Jednostka* jedn);
    Jednostka* dodaj(Jednostka *jedn);
};

//Klasa opisująca jednostki pochodne
class Pochodna : public Jednostka {
private:
//    string skrot;
//    string nazwa;
//    string nazwaMiedzynarodowa;
    Podstawowa *skladowe;
    int iloscSkladowych; //rozmiar tablicy

    void inicjujSkladowe();
    bool czyZawiera(const Podstawowa *sprawdzana) const;
    void dodajSkladowa(Podstawowa *podstawowa);
    void generujOpis();
protected:
public:
    Pochodna();
    Pochodna(string _nazwa, string _skrot, string _nazwaMiedzynarodowa);
    Pochodna(const Pochodna &poch);
    ~Pochodna();
    string toString();


    void modyfikujSkladowa(Podstawowa * nowa);

    bool czyMoznaSumowac(Jednostka *jedn);

    Jednostka* pomnoz(Jednostka* jedn);
    Jednostka* dodaj(Jednostka *jedn);

    Podstawowa* getSkladowe();
};

//definicja domyślnego typu dla wartosci
typedef WzorzecWartosci<long double, Jednostka*> TYP_WARTOSCI;

class Wartosc : public TYP_WARTOSCI {
public:
    TYP_WARTOSCI* pomnoz(TYP_WARTOSCI *wart);
    TYP_WARTOSCI* dodaj(TYP_WARTOSCI *wart);

    Wartosc* pomnoz(Wartosc *wart);
    Wartosc* dodaj(Wartosc *wart);

    Jednostka* getJednostka();
    void setJednostka(Jednostka* jedn);
    long double getWartosc();
    void setWartosc(long double wartosc);

    Wartosc operator*(long double wart);
    Wartosc operator*(Wartosc wart);
    Wartosc operator/(long double wart);
    Wartosc operator/(Wartosc wart);
    Wartosc operator+(Wartosc wart);
    Wartosc operator-(Wartosc wart);
    Wartosc operator^(int wykl);
    Wartosc operator=(TYP_WARTOSCI* wart);
    Wartosc operator-();

    operator TYP_WARTOSCI*() {
        return this;
    }


    virtual ~Wartosc() { delete &jednostka; }
};

}


#endif // INTERFEJSY_H
