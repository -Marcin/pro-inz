#ifndef LITERALY_H
#define LITERALY_H

#include "jednostkiSI.h"
#include "wartosciSI.h"

using namespace SI;


Wartosc operator "" s(long double _wart) {
    Wartosc  wart = Wartosc();
    wart.setJednostka(new Czas());
    wart.setWartosc(_wart);
    return wart;
}

Wartosc operator"" m(long double _wart) {
    Wartosc  wart = Wartosc();
    wart.setJednostka(new Dlugosc());
    wart.setWartosc(_wart);
    return wart;
}

Wartosc operator"" kg(long double _wart) {
    Wartosc  wart = Wartosc();
    wart.setJednostka(new Masa());
    wart.setWartosc(_wart);
    return wart;
}

Wartosc operator"" K(long double _wart) {
    Wartosc  wart = Wartosc();
    wart.setJednostka(new Temperatura());
    wart.setWartosc(_wart);
    return wart;
}

Wartosc operator"" A(long double _wart) {
    Wartosc  wart = Wartosc();
    wart.setJednostka(new Prad());
    wart.setWartosc(_wart);
    return wart;
}

Wartosc operator"" mol(long double _wart) {
    Wartosc  wart = Wartosc();
    wart.setJednostka(new LicznoscMaterii());
    wart.setWartosc(_wart);
    return wart;
}

Wartosc operator"" cd(long double _wart) {
    Wartosc wart = Wartosc();
    wart.setJednostka(new Swiatlosc());
    wart.setWartosc(_wart);
    return wart;
}


#endif // LITERALY_H
