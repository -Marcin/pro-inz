#ifndef WYJATKISI_H
#define WYJATKISI_H
#include <iostream>
#include <cstring>
#include <ostream>
using namespace std;

namespace SI {
    class Wyjatek {
    private:
        string zrodlo;
    public:
        Wyjatek() {}
        Wyjatek(string _prz):zrodlo(_prz) {}

        virtual void wyswietl() {
            cout << "\nWYJATEK";
            cout << "\nZRODLO:\t";
            cout << zrodlo;
        }

    };

    class NiedozwolonaOperacja : public Wyjatek {
    private:
        virtual void wyswietl() {
            Wyjatek::wyswietl();
        }

    public:
        NiedozwolonaOperacja() {}
        NiedozwolonaOperacja(string zrodlo) : Wyjatek(zrodlo) {}
    };
    class NiepoprawnyTypObiektu : public Wyjatek {
    private:
        string oczekiwano;
    public:
        NiepoprawnyTypObiektu() {}
        NiepoprawnyTypObiektu(string zrodlo, string oczekiwano = "") : Wyjatek(zrodlo), oczekiwano(oczekiwano) {}
        void wyswietl() {
            Wyjatek::wyswietl();
            cout << "\nOCZEKIWANO:\t";
            cout << oczekiwano << endl;
        }
    };
    class RozneWykladniki : public NiedozwolonaOperacja {
    public:
        RozneWykladniki() {}
        RozneWykladniki(string zrodlo) : NiedozwolonaOperacja(zrodlo) {}
    };
}
#endif // WYJATKISI_H
