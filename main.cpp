#include "jednostkiSI.h"
#include "zdefiniowane.h"
#include "wartosciSI.h"
#include "literaly.h"
#include <typeinfo>
#include <assert.h>

//#define NDEBUG

using namespace std;



void testWykladnikow() {
    cout << "\n\n------Test wykladnikow: " << endl;
    SI::Czas t = SI::Czas();
    cout << "\nDomyslny wykladnik: " << t.getWykladnik();
    t^2;
    cout << "\nPo wywolaniu Czas^2: " << t ;
    t^2;
    cout << "\nPo wywolaniu Czas^2: " << t;
    t^4;
    cout << "\nPo wywolaniu Czas^4: " << t;
    t^10;
    cout << "\nPo wywolaniu Czas^10: " << t <<endl;
}

void testOperatorow() {
    cout << "\n\n------Test operatorow: " << endl;

    cout << "10.0m + 5.0m = " << 10.0m + 5.0m;
}

void testDodawania() {
//    cout << "\n\n------Test dodawania: " << endl;
//    try {
//        cout << "20cd + 10cd = " << (20.0cd).dodaj(10.0cd);
//        cout << "tu wystapi wyjatek -> " << (12.0K).dodaj(1.0m);
//    } catch (Wyjatek* ex) {
//        ex->wyswietl();
//    }
}

void testLiteralow() {
    Wartosc wartosc;
    wartosc = 0.0m;
    assert(dynamic_cast<Dlugosc*>(wartosc.getJednostka()));
    wartosc = 0.0s;
    assert(dynamic_cast<Czas*>(wartosc.getJednostka()));
    wartosc = 0.0K;
    assert(dynamic_cast<Temperatura*>(wartosc.getJednostka()));
    wartosc = 0.0cd;
    assert(dynamic_cast<Swiatlosc*>(wartosc.getJednostka()));
    wartosc = 0.0A;
    assert(dynamic_cast<Prad*>(wartosc.getJednostka()));
    wartosc = 0.0mol;
    assert(dynamic_cast<LicznoscMaterii*>(wartosc.getJednostka()));
    wartosc = 0.0kg;
    assert(dynamic_cast<Masa*>(wartosc.getJednostka()));

    wartosc = 100.0m;
    assert(wartosc.getWartosc() == 100.0);
    wartosc = -5.0kg;
    assert(wartosc.getWartosc() == -5.0);

    assert(!dynamic_cast<Czas*>(wartosc.getJednostka()));
    assert(!dynamic_cast<Prad*>(wartosc.getJednostka()));

    cout << "Test literalow przebiegl pomyslnie!";
}

int main() {
//    testWykladnikow();
//    testDodawania();
//    testMnozenia();
//    cout << max(1, 2);
//    cout << max(10.0, 20.0);
//    cout << max(120.0f, 12.0f);
//    cout << max ('a', 'z');
//    WzorzecWartosci<long double, Jednostka*>* w = new Wartosc();
//    delete w;
    testLiteralow();
    testOperatorow();
}

