#ifndef J_SI_PODST
#define J_SI_PODST

#include <iostream>
#include <cstring>
#include <sstream>
#include <typeinfo>
#include "WyjatkiSI.h"
#include "interfejsy.h"

using namespace std;

//implementacja w kolejnych identycznych namespace'ach, łatwiej się można poruszać w edytorze
//bazowa
namespace SI {

    Jednostka::Jednostka(string _nazwa, string _skrot, string _nazwaMiedzynarodowa)
        : nazwa(_nazwa),
          skrot(_skrot),
          nazwaMiedzynarodowa(_nazwaMiedzynarodowa),
          wykladnik(1) {}

    string Jednostka::toString() {
        ostringstream sout;
        if (this->wykladnik != 0) {
            sout << this->skrot;
            if (this->wykladnik != 1)  {
                sout << "^" << this->wykladnik;
            }
        }
        return sout.str();
    }


    Jednostka::Jednostka() {}
    Jednostka::~Jednostka() {}

    Jednostka::Jednostka(const Jednostka &jedn) {
        this->nazwa = jedn.nazwa;
        this->nazwaMiedzynarodowa = jedn.nazwaMiedzynarodowa;
        this->skrot = jedn.skrot;
    }
    string Jednostka::getNazwa() {
        return nazwa;
    }
    string Jednostka::getSkrot() {
        return skrot;
    }
    int Jednostka::getWykladnik() {
        return wykladnik;
    }
    void Jednostka::setWykladnik(int _wykladnik) {
        wykladnik = _wykladnik;
    }

    bool Jednostka::czyMoznaSumowac(Jednostka *jedn) {
        return this->wykladnik == jedn->wykladnik;
    }

    //przeciazanie operatora XOR, by sluzyl jako potega (do zmiany stopnia wykladnika)
    Jednostka* Jednostka::operator^ (int _wykladnik) {
        this->wykladnik = this->wykladnik*_wykladnik;
        return this;
    }

    //przeciazenie oparatora << dla cout, klasa bedzie wyswietlana jako skrot[^wykladnik]
    ostream &operator<< (ostream &wyjscie, Jednostka &jedn) {

        return wyjscie << jedn.toString();
    }
    ostream &operator<< (ostream &wyjscie, Jednostka* jedn) {
        return wyjscie << jedn->toString();
    }

    Jednostka* Jednostka::pomnoz(Jednostka *jedn) {
        this->nazwa = "";
        this->skrot = this->toString() + jedn->toString();
        this->nazwaMiedzynarodowa = "";
        return this;
    }

    //operatory arytmetyczne
    Jednostka* Jednostka::operator*(Jednostka &jedn) {
        return this->pomnoz(&jedn);
    }
    Jednostka* Jednostka::operator/(Jednostka &jedn) {
        return this->pomnoz(jedn^-1);
    }
    //dodawanie i odejmowanie sprawdza tylko, czy jednostki są identyczned
    Jednostka* Jednostka::operator+(Jednostka &jedn) {
        if (!this->czyMoznaSumowac(&jedn))
            throw new RozneWykladniki();
        return this;
    }
    Jednostka* Jednostka::operator-(Jednostka &jedn) {
        if (!this->czyMoznaSumowac(&jedn))
            throw new RozneWykladniki();
        return this;
    }

}
//PODSTAWOWA
namespace SI {
    Podstawowa::Podstawowa(string _nazwa, string _skrot, string _nazwaMiedzynarodowa = "")
        : Jednostka(_nazwa, _skrot,_nazwaMiedzynarodowa){}
    Podstawowa::Podstawowa(const Podstawowa &podstawowa) : Jednostka(podstawowa){}

    Podstawowa::Podstawowa() {}

    //operator porownania
    bool Podstawowa::operator== (const Podstawowa *podstawowa) const {
        return this->wykladnik == podstawowa->wykladnik && this->is(podstawowa);
    }

    //metoda sprawdzajaca czy jednostka jest tego samego typu
    bool Podstawowa::is(const Podstawowa *podstawowa) const {
        return this->getId() == podstawowa->getId();
    }


    Podstawowa* Podstawowa::operator=(Podstawowa &jedn) {
        return new Podstawowa(jedn);
    }

    Podstawowa* Podstawowa::operator=(Podstawowa *jedn) {
        return new Podstawowa(*jedn);
    }

    //operator mnożenia dwóch jednostek podstawowych, jeżeli jednostki są różne tworzy jednostkę pochodną
    Jednostka* Podstawowa::pomnoz(Jednostka* jedn)  {
        if (Podstawowa *skladnik = dynamic_cast<Podstawowa*>(jedn)) {
            if (this->is(skladnik)) {
                Podstawowa *nowa = new Podstawowa(*this);
                nowa->wykladnik = this->wykladnik + skladnik->wykladnik;
                return nowa;
            }
            Pochodna *nowa = new Pochodna();
            nowa->modyfikujSkladowa(this);
            nowa->modyfikujSkladowa(skladnik);
            return nowa;
        } else if (Pochodna *skladnik = dynamic_cast<Pochodna*>(jedn)) {
            return skladnik->pomnoz(this);
        }
        throw new NiepoprawnyTypObiektu("Podstawowa::pomnoz(Jednostka*)", "Jednostka");
    }

    Jednostka* Podstawowa::dodaj(Jednostka *jedn) {
        if (!Jednostka::czyMoznaSumowac(jedn))
            throw new RozneWykladniki();
        if (Podstawowa *sparta = dynamic_cast<Podstawowa*>(jedn))
           if (this->is(sparta)) return this;
        throw new NiepoprawnyTypObiektu("Podstawowa::dodaj(Jednostka*)", typeid(jedn).name());
    }

}

//POCHODNA
namespace SI {

    void Pochodna::inicjujSkladowe() {
        if (0 == this->iloscSkladowych) {
            this->skladowe = new Podstawowa[1];
        } else {
            int nowyRozmiar = iloscSkladowych + 1;
            Podstawowa *tmp = new Podstawowa[nowyRozmiar];
            copy(this->skladowe, this->skladowe+iloscSkladowych, tmp);
            delete this->skladowe;
            this->skladowe = tmp;
        }
    }

    bool Pochodna::czyZawiera(const Podstawowa *sprawdzana) const {
        if (iloscSkladowych == 0) return false;
        for (int i = 0; i < iloscSkladowych; i++)
            if (skladowe[i].is(sprawdzana))
                return true;
        return false;
    }

    void Pochodna::dodajSkladowa(Podstawowa *podstawowa) {
        if (!czyZawiera(podstawowa)) {
            inicjujSkladowe();
            this->skladowe[iloscSkladowych++] = new Podstawowa(*podstawowa);
        } else {
            modyfikujSkladowa(podstawowa);
        }
        this->generujOpis();
    }

    void Pochodna::generujOpis() {
        this->skrot = "";
        for (int i = 0; i < this->iloscSkladowych; i++) {
            if (i > 0) skrot += "*";
            this->skrot += this->skladowe[i].toString();
        }
        this->nazwa = "";
        this->nazwaMiedzynarodowa = "";
    }

    string Pochodna::toString() {
        ostringstream sout;
        this->generujOpis();
        if (!skrot.empty()) {
            sout << "*";
        if (wykladnik != 1) sout << "(";
            sout << skrot;
        if (wykladnik != 1) sout << ")^" << wykladnik;

        }
        return sout.str();
    }


    Pochodna::Pochodna() : Jednostka() { this->iloscSkladowych = 0;}

    Pochodna::Pochodna(string _nazwa, string _skrot = "", string _nazwaMiedzynarodowa = "")
        : Jednostka(_nazwa, _skrot, _nazwaMiedzynarodowa) {}


    Pochodna::Pochodna(const Pochodna &poch) : Jednostka(poch) {
        this->iloscSkladowych = poch.iloscSkladowych;
        this->skladowe = new Podstawowa[iloscSkladowych];
        for (int i = 0; i < iloscSkladowych; i++) {
            this->skladowe[i] = poch.skladowe[i];
        }
    }

Pochodna::~Pochodna() {
    delete [] this->skladowe;
}

void Pochodna::modyfikujSkladowa(Podstawowa *nowa) {
    for (int i = 0; i < iloscSkladowych; i++) {
        if (skladowe[i].is(nowa)) {
            skladowe[i].pomnoz(nowa);
            generujOpis();
            return;
        }
    }
    inicjujSkladowe();
    this->skladowe[iloscSkladowych++] = new Podstawowa(*nowa);
    generujOpis();
}

bool Pochodna::czyMoznaSumowac(Jednostka *skladnik) {
    bool wynik;// = Jednostka::czyMoznaSumowac(skladnik);
    if (Podstawowa *podst = dynamic_cast<Podstawowa*>(skladnik)) {
        wynik = this->czyZawiera(podst) && this->iloscSkladowych == 1;
    } else if (Pochodna *poch = dynamic_cast<Pochodna*>(skladnik)) {
        for (int i = 0; i < this->iloscSkladowych; i++) {
            wynik = wynik && this->skladowe[i].czyMoznaSumowac(poch);
            if (!wynik) {
                return false;
            }
        }
    }
    return wynik;
}

Jednostka* Pochodna::pomnoz(Jednostka *jedn) {
    if (Podstawowa *skladnik = dynamic_cast<Podstawowa*>(jedn)) {
        this->modyfikujSkladowa(skladnik);
    } else if (Pochodna *skladnik = dynamic_cast<Pochodna*>(jedn)) {
        for (int i = 0; i < skladnik->iloscSkladowych; i++) {
            this->modyfikujSkladowa(&skladnik->skladowe[i]);
        }
    } else {
        throw new NiepoprawnyTypObiektu();
    }
    generujOpis();
    return this;
}

Jednostka* Pochodna::dodaj(Jednostka *skladnik) {
    if (this->czyMoznaSumowac(skladnik)) {
        return this;
    }
    throw new NiepoprawnyTypObiektu("Pochodna::dodaj(Jednostka*)", typeid(skladnik).name());
}

Podstawowa* Pochodna::getSkladowe() {
    return this->skladowe;
}

}


#endif
