#ifndef WARTOSCISI_H
#define WARTOSCISI_H
#include "interfejsy.h"
#include "jednostkiSI.h"
#include "WyjatkiSI.h"
#include <iostream>
#include <cmath>
using namespace std;

namespace SI {

template <typename W, typename J>
string WzorzecWartosci<W, J>::toString() {
     ostringstream sout;
     sout << wartosc;
     sout << jednostka;
     return sout.str();
 }
template<typename W, typename J>
W WzorzecWartosci<W, J>::getWartosc() {
    return this->wartosc;
}

template<typename W, typename J>
void WzorzecWartosci<W, J>::setWartosc(W wartosc) {
    this->wartosc = wartosc;
}


template<typename W, typename J>
J WzorzecWartosci<W, J>::getJednostka() {
    return this->jednostka;
}

template<typename W, typename J>
void WzorzecWartosci<W, J>::setJednostka(J jedn) {
    this->jednostka = jedn;
}

template <typename W, typename J>
WzorzecWartosci<W, J>::~WzorzecWartosci() {
//    delete jednostka;
}

//WARTOSC

Jednostka* Wartosc::getJednostka() {
    return this->jednostka;
}

void Wartosc::setJednostka(Jednostka *jedn) {
    this->jednostka = jedn;
}

long double Wartosc::getWartosc() {
    return this->wartosc;
}

void Wartosc::setWartosc(long double wartosc) {
    this->wartosc = wartosc;
}

TYP_WARTOSCI* Wartosc::pomnoz(TYP_WARTOSCI *_wart) {
    if (Wartosc *wart = dynamic_cast<Wartosc*>(_wart)) {
        return this->pomnoz(wart);
    }
    throw new NiepoprawnyTypObiektu("Wartosc::pomnoz(WzorzecWartosci<long double, Jednostka>*)", "Wartosc");
}

Wartosc* Wartosc::pomnoz(Wartosc *_wart) {
    if (Wartosc *wart = dynamic_cast<Wartosc*>(_wart)) {
        this->jednostka->pomnoz(wart->getJednostka());
        this->wartosc *= wart->wartosc;
        return this;
    }
    throw new NiepoprawnyTypObiektu("Wartosc::pomnoz(WzorzecWartosci<long double, Jednostka>*)", "Wartosc");
}

TYP_WARTOSCI* Wartosc::dodaj(TYP_WARTOSCI *_wart) {
    if (Wartosc *wart = dynamic_cast<Wartosc*>(_wart)) {
        return this->dodaj(wart);
    }
    throw new NiepoprawnyTypObiektu("Wartosc::dodaj(WzorzecWartosci<long double, Jednostka>*)", "Wartosc");
}

Wartosc* Wartosc::dodaj(Wartosc* _wart) {
    if (Wartosc *wart = dynamic_cast<Wartosc*>(_wart)) {
        //jezeli operacja jest niemozliwa - zostanie rzucony wyjatek
        this->jednostka->dodaj(wart->jednostka);
        this->wartosc += wart->wartosc;
        return this;
    }
    throw new NiepoprawnyTypObiektu("Wartosc::dodaj(WzorzecWartosci<long double, Jednostka>*)", "Wartosc");
}

Wartosc Wartosc::operator*(long double wart) {
    this->wartosc *= wart;
    return *this;
}

Wartosc Wartosc::operator/(long double wart) {
    this->wartosc /= wart;
    return *this;
}

Wartosc Wartosc::operator*(Wartosc wart){
    return *this->pomnoz(&wart);
}

Wartosc Wartosc::operator/(Wartosc wart){
    this->wartosc = 1.0 / this->wartosc;
    *this->jednostka^-1;
    return *this->pomnoz(&wart);
}

Wartosc Wartosc::operator+(Wartosc wart){
    this->jednostka->dodaj(wart.getJednostka());
    this->wartosc += wart.getWartosc();
    return *this->dodaj(&wart);
}
Wartosc Wartosc::operator-(Wartosc wart){
    this->jednostka->dodaj(wart.getJednostka());
    this->wartosc += wart.getWartosc();
    return *this;
}

Wartosc Wartosc::operator^(int wykl){
    *this->getJednostka()^wykl;
    this->wartosc = pow(wartosc, wykl);
    return *this;
}

Wartosc Wartosc::operator-() {
    this->wartosc *= -1;
    return *this;
}

}

#endif // WARTOSCISI_H
